# Fedora:
- [Sch:](https://www.google.com/search?q=fedora+Live+USB+Persistence)

alt:
- https://docs.pagure.org/docs-fedora/how-to-create-and-use-live-usb.html

## Tool: Livecd-iso-to-disk
Top.doc!
- https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/#using-the-livecd-iso-to-disk-tool

more:
- https://fedoraproject.org/wiki/Livecd-iso-to-disk

### Example:
```
usb_disk=sdX

livecd-iso-to-disk -O 64bit --force --format --reset-mbr --home-size-md 2048 --overlay-size-mb 1024 Fedora-LXDE-Live-x86_64-32_Beta-1.2.iso /dev/$usb_disk
```