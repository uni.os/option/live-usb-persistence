# Live USB Persistence
Install Env on a USB with persistence!

## Features:
- copy & mount highly volatile files on a Disk to reduce wear on USB.